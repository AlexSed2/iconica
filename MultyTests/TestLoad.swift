import XCTest
import Network
import Files

@testable import Multy

class LoadTest: XCTestCase {
    func test() async {
        do {
            let shape = try await GetAlbumsShape().runAsync()
            let model = Model(shape: shape)
            print("shape: \(model.stableSorted)")
        } catch {
        }
    }
    
    func testIterator() async {
        do {
            let shape = try await GetAlbumsShape().runAsync()
            var model = Model(shape: shape)
            let automatic = try await Automatic(eachLenth: 5)
            for try await icon in automatic {
                await Task.sleep(500_000_000)
                let data = icon.image.data(using: .utf8)
                let not = await fill(badIcon: icon, data: data!)
                model.insert(notSoBadIcon: not)
                print(model.stableSorted)
            }
        } catch {
            print("no automatic")
        }
    }
    
    func loadAtOnce() {
        
    }
    
    @MainActor func fill(badIcon: BadIcon, data: Data) -> NotSoBadIcon {
        var not = NotSoBadIcon(badIcon: badIcon)
        let image = SVGKFastImageViewSUI(data: data, size: CGSize(width: 10, height: 10))
        not.image = image
        return not
    }
}
