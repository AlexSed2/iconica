import Network
import Darwin


struct Automatic<Element>: AsyncIteratorProtocol {
    
    var store: [Element]
    
    mutating func next() async throws -> Element? {
        await Task.sleep(500_000_000)
        guard store.count > 0 else { return nil }
        return store.removeLast()
    }
    
}

struct AutomaticStore<Element>: AsyncSequence {
    
    typealias AsyncIterator = Automatic<Element>
    typealias Element = Element
    
    var collection: [Element]
    
    func makeAsyncIterator() -> Automatic<Element> {
        Automatic(store: collection)
    }
    
}

struct PrepareFace {
    func dummy() async -> [NotSoBadAlbum] {
        return (0..<25).map { NotSoBadAlbum(number: $0) }
    }
    func prepare(albums: [BadAlbum]) -> [NotSoBadAlbum] {
        let slicedalbums = albums.map { album -> BadAlbum in
            var album = album
            album.icons = Array(album.icons[..<60])
            return album
        }
        let current = slicedalbums.map { NotSoBadAlbum(badAlbum: $0) }
        return current
    }
}
