import Network
import Combine
import SVGKit


@MainActor
class Controller: ObservableObject {
    
    static let shared = Controller()
    
    static func dummmySetupSequence() {
        Task.detached {
            let face = await PrepareFace().dummy()
            let sequence = AutomaticStore(collection: face)
            for try await album in sequence {
                var model = await shared.model
                model.append(album: album)
                await shared.set(model: model)
            }
        }
    }
    
    
    
    static func fill() {
        guard shared.model.albums.count < 10 else {
            print("RETURN")
  //          print("model: \(shared.model.albums)")
            return
        }
        Task.detached {
            do {
                let shape = try await GetAlbumsShape().runAsync()
                var model = Model(shape: shape)
                await shared.set(model: model)
                let automatic = try await Network.Automatic(eachLenth: 60)
                
                var bunch = [NotSoBadIcon]()
                
                for try await icon in automatic {
                    
                    let data = icon.image.data(using: .utf8)
                    let not = await fill(badIcon: icon, data: data!)
                    
                    bunch.append(not)
                    
                   if not.id < 60 * 2 {
                        if not.id % 10 == 0 {
                            model = await shared.model
                            model.insert(bunch: bunch)
                            bunch = []
                            await shared.set(model: model)
                            await Task.sleep(400_000_000)
                        }
                    } else {
                        if not.id % 60 == 0 {
                            model = await shared.model
                            model.insert(bunch: bunch)
                            bunch = []
                            await shared.set(model: model)
                            await Task.sleep(1000_000_000)
                        }
                    }
                }
            } catch let err {
                print("error: \(err)")
            }
        }
    }
    
    static var currentID = 1
    @MainActor static func fill(badIcon: BadIcon, data: Data) -> NotSoBadIcon {
        var not = NotSoBadIcon(badIcon: badIcon)
        let image = SVGKFastImageViewSUI(data: data, size: CGSize(width: 10, height: 10))
        not.image = image
        not.id = currentID
        currentID += 1
        return not
    }
    
    @Published var model = Model()
    func set(model: Model) { self.model = model }
    func select(icon: NotSoBadIcon) { model.select(icon: icon) }
    
}
