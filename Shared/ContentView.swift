//
//  ContentView.swift
//  Shared
//
//  Created by Alexander Sedykh on 13.10.2021.
//

import SwiftUI
import Network

struct ContentView: View {
    @State private var bottomSheetShown = false
    @State var selectedPucture: NotSoBadIcon?
    
    let controller = Controller.shared
    
    var body: some View {
        GeometryReader { geometry in
            Canvas(icon: selectedPucture).onTapGesture { print("tap") }
            BottomSheetView(isOpen: self.$bottomSheetShown, maxHeight: geometry.size.height * 0.7) {
                GridView(selectedPicture: $selectedPucture).environmentObject(controller)
            }.shadow(radius: 10)
        }.edgesIgnoringSafeArea(.all).onAppear {
//            Controller.dummmySetupSequence()
            Controller.fill()
        }
    }
}

struct Canvas: View {
    var icon: NotSoBadIcon?
    var body: some View {
        if let icon = icon {
            icon.image
        } else {
            Rectangle().fill(Color.orange)
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
