import SwiftUI
import Network
import Algorithms


struct GridView: View {
    
    @EnvironmentObject var controller: Controller
    @Binding var selectedPicture: NotSoBadIcon?
    
    var body: some View {
        let k =
        ScrollView {
            VStack {
                ForEach(controller.model.stableSorted) { album in
                    
                    if album.icons.isEmpty == false {
                        Text(album.title).font(Font.footnote)
                        Divider()                        
                    }
                    VStack(alignment: .leading, spacing: 0) {
                        ForEach(album.rows) { row in
                            HStack {
                                ForEach(row.icons) { icon in
                                    Cell(model: icon).onTapGesture {
                                        controller.select(icon: icon)
                                        selectedPicture = icon
                                    }
                                }
                            }
                        }
                    }
                }
            }
      
        }
        return k
    }
}


struct Cell: View {
    let model: NotSoBadIcon
    var body: some View {
        ZStack {
            let color = Color(white: 0.9)
            let rect = model.rect.fill(model.select == .selected ? Color.green : color).cornerRadius(10)
            if let image = model.image {
                image.frame(width: 25, height: 25).background(rect)
            } else {
                model.rect.fill(model.select == .selected ? Color.green : Color.orange).cornerRadius(10).frame(width: 25, height: 25)
            }
        }
    }
}
