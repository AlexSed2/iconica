import Foundation
import CoreImage
import SwiftUI
import Network
import Algorithms

enum Select { case selected, free }
protocol Selectable { var select: Select { get set } }

struct Model: Equatable {
    var albums: Set<NotSoBadAlbum>
    var stableSorted: [NotSoBadAlbum] { albums.sorted() }
    init() {
        
        let shape = Fetcher().readAllBadAlbumsSync()
        albums = Set(shape.map { bad in
            var bad = bad
            bad.icons = Array(bad.icons.prefix(60))
            var z = NotSoBadAlbum(badAlbum: bad)
            let icons = bad.icons
            let noticons = icons.map { icon -> NotSoBadIcon in
                let data = icon.image.data(using: .utf8)
                let image = SVGKFastImageViewSUI(data: data!, size: CGSize(width: 10, height: 10))
                var not = NotSoBadIcon(badIcon: icon)
                not.image = image
                return not
            }
            z.icons = noticons
            print("z: \(z.icons.count)")
            return z
        })
        print("count: \(albums.count)")
    }
    init(shape: [Album]) {
        albums = Set(shape.map { NotSoBadAlbum(album: $0) })
    }
    mutating func append(album: NotSoBadAlbum) {
        albums.insert(album)
    }
    mutating func deselect() {
        for album in albums {
            var a = album
            a.deselectAll()
            albums.update(with: a)
        }
    }
    mutating func select(icon: NotSoBadIcon) {
        deselect()
        guard var a = albums.first(where: { album in
            album.icons.contains(icon)
        }) else { return }
        a.select(icon: icon)
        albums.update(with: a)
    }
    mutating func insert(notSoBadIcon: NotSoBadIcon) {
        guard var a = albums.first(where: { album in
            album.id == notSoBadIcon.pack
        }) else { return }
        a.icons.append(notSoBadIcon)
        albums.update(with: a)
    }
    mutating func insert(bunch: [NotSoBadIcon]) {
        guard bunch.isEmpty == false else { return }
        guard var a = albums.first(where: { album in
            album.id == bunch[0].pack
        }) else { return }
        a.icons += bunch
        albums.update(with: a)
    }
}

struct NotSoBadAlbum: Equatable, Hashable, Identifiable, Comparable {
    
    let id: Int
    let title: String
    var icons: [NotSoBadIcon]
    
    struct Row<C: RandomAccessCollection>: Identifiable,  Equatable {
        let icons: C
        let id: Int
        static func == (lhs: Row, rhs: Row) -> Bool {
            lhs.id == rhs.id
        }
    }
    
    var rows: [Row<ArraySlice<NotSoBadIcon>>] {
        var rows = [Row<ArraySlice<NotSoBadIcon>>]()
        for (index, item) in icons.chunks(ofCount: 10).enumerated() {
            rows.append(Row(icons: item, id: index + id * index))
        }
        return rows
    }
    
    
    init(number: Int) {
        id = number
        title = ""
//        icons = (1...10).map { NotSoBadIcon(id: $0 + 10 * number) }
        icons = (1...10).map {_ in NotSoBadIcon(id: UUID().hashValue) }
    }
    
    init(badAlbum: BadAlbum) {
        id = badAlbum.id
        title = badAlbum.title
        icons = badAlbum.icons.map { NotSoBadIcon(badIcon: $0) }
    }
    
    init(album: Album) {
        id = album.id
        title = album.name
        icons = []
    }
    
    mutating func deselectAll() {
        icons = icons.map { item in
            var t = item
            t.select = .free
            return t
        }
    }
    
    mutating func select(icon: NotSoBadIcon) {
        icons = icons.map { item in
            var t = item
            t.select = .free
            if t == icon {
                t.select = .selected
            }
            return t
        }
    }
    
    static func == (lhs: NotSoBadAlbum, rhs: NotSoBadAlbum) -> Bool {
        lhs.id == rhs.id
    }
    
    static func < (lhs: NotSoBadAlbum, rhs: NotSoBadAlbum) -> Bool {
        lhs.id < rhs.id
    }
    
    func hash(into hasher: inout Hasher) {
           hasher.combine(id)
    }
}

struct NotSoBadIcon: Equatable, Hashable, Identifiable, CustomStringConvertible {
    
    
    var id: Int
    let pack: Int
    let title: String
    var rect: Rectangle
    var image: SVGKFastImageViewSUI?
    var select: Select = .free
    
    var description: String {
        switch select {
        case .selected: return "♢"
        case .free: return "♦︎"
        }
    }

    init(id: Int) {
        self.id = id
        self.title = "icon"
        self.rect = Rectangle()
        self.pack = Int.random(in: 0...Int.max)
    }
    
    init(badIcon: BadIcon) {
        id = badIcon.id
        title = badIcon.title
        rect = Rectangle()
        pack = badIcon.pack
    }
    
    static func == (lhs: NotSoBadIcon, rhs: NotSoBadIcon) -> Bool {
        lhs.id == rhs.id && lhs.select == rhs.select
    }
    
    func hash(into hasher: inout Hasher) {
           hasher.combine(id)
    }
}
