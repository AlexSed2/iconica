import SVGKit
import Network
import SwiftUI


import SwiftUI
import SVGKit

#if os(iOS)
typealias ViewRepresanteble = UIViewRepresentable
#else
typealias ViewRepresanteble = NSViewRepresentable
#endif

struct SVGKFastImageViewSUI: ViewRepresanteble {
    
    let data: Data
    let size: CGSize
    let svgImage: SVGKImage
    let fastView: SVGKFastImageView
    
    init(data: Data, size: CGSize) {
        self.data = data
        self.size = size
        let svgimage = SVGKImage(data: data)
        self.fastView = SVGKFastImageView(svgkImage: svgimage)
        self.svgImage = svgimage!
    }
    
    func makeUIView(context: Context) -> SVGKFastImageView {
        fastView
    }
    func makeNSView(context: Context) -> SVGKFastImageView {
        fastView
    }
    func updateUIView(_ uiView: SVGKFastImageView, context: Context) {
        uiView.image = svgImage
        uiView.image.size = size
    }
    func updateNSView(_ uiView: SVGKFastImageView, context: Context) {
        uiView.image = svgImage
        uiView.image.size = size
    }
    
}

extension BadIcon {
    
    var svgImage: SVGKFastImageViewSUI {
        let data = image.data(using: .utf8)
        return SVGKFastImageViewSUI(data: data!, size: CGSize(width: 100, height: 100))
    }
    
}

