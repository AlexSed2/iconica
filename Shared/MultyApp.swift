//
//  MultyApp.swift
//  Shared
//
//  Created by Alexander Sedykh on 13.10.2021.
//

import SwiftUI

@main
struct MultyApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
//            RootView()
        }
    }
}
